package com.fferreira3d;

import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class SpringSeboAplicacaoWebApplication implements CommandLineRunner {
	
	private static String MICRO_SERVICE_CLIENTE = "http://localhost:8888/cliente";
	private static String MICRO_SERVICE_LIVRO = "http://localhost:9999/livro";
	private static String MICRO_SERVICE_AMIGO = "http://localhost:7777/amigo";
	private static String MICRO_SERVICE_EMPRESTIMO = "http://localhost:6666/emprestimo";
	
	public static void main(String[] args) {
		SpringApplication.run(SpringSeboAplicacaoWebApplication.class, args);
	}
	@Override
	public void run(String... args) throws Exception {
		System.out.println("----------------------------");
		System.out.println("SEBO VIRTUAL - Aplicação Web");
		System.out.println("----------------------------");
		
		try {
			System.out.println("Persistindo dados de exemplo...");
			Cliente clienteFelipe = new Cliente("Felipe Ferreira","fferreira3d@gmail.com");
			Cliente clienteJoao = new Cliente("João Silva","jo_silva@gmail.com");
			Cliente clientePaulo = new Cliente("Paulo Henrique","paulo_heri@gmail.com");
			
			criarCliente(clienteFelipe.name, clienteFelipe.email);
			criarCliente(clienteJoao.name, clienteJoao.email);
			criarCliente(clientePaulo.name, clientePaulo.email);
			
			adicionarLivro(new Livro("Senhor dos aneis","Autor 1","Editora 1",clienteFelipe.email), clienteFelipe);
			adicionarLivro(new Livro("Eragon","Paolini","Editora 2",clienteFelipe.email), clienteFelipe);
			adicionarLivro(new Livro("Curso Node.js","Autor 3","Editora 3",clienteFelipe.email), clienteFelipe);
			
			solicitarAmizade(clientePaulo.email, clienteFelipe.email);
			solicitarAmizade(clientePaulo.email, clienteJoao.email);
			solicitarAmizade(clientePaulo.email, clientePaulo.email);
			solicitarEmprestimo(clientePaulo, meusLivros(clienteFelipe).get(0));
			
			System.out.println("");
			
			String logMens = "";
			List<Cliente> listaDeClientes = getTodosOsClientes();
			List<Livro> listaDeLivros = getTodosOsLivros();
			List<Amigo> listaDeAmigos = getListaDeAmigos(listaDeClientes.get(0));
			List<Emprestimo> listaDeEmprestimos = meusEmprestimos(listaDeClientes.get(0));
			
			logMens += "\n-----------------------\n";
			
			logMens += "Lista de Clientes:\n";
			for(Cliente cliente : listaDeClientes)
				logMens += "* " + cliente.name + "\n";
			
			logMens += "\n-----------------------\n";
			
			logMens += "Lista de Livros:\n";
			for(Livro livro : listaDeLivros)
				logMens += "* " + livro.titulo + "\n";
			
			logMens += "\n-----------------------\n";
			
			logMens += "Lista de Amigos de " + listaDeClientes.get(0).name + ":\n";
			for(Amigo amigo : listaDeAmigos) {
				if(amigo.emailDestino.equalsIgnoreCase(listaDeClientes.get(0).email))
					logMens += "* " + getCliente(amigo.emailOrigem).name + "\n";
				else
					logMens += "* " + getCliente(amigo.emailDestino).name + "\n";
			}
			
			logMens += "\n-----------------------\n";
			
			logMens += "Lista de Emprestimos de " + listaDeClientes.get(0).name + ":\n\n";
			for(Emprestimo emprestimo : listaDeEmprestimos) {
				logMens += "* " + getLivro(emprestimo.idDoLivro).titulo + "\n";
			}
			System.out.println(logMens);
		} catch(ResourceAccessException e) {
			System.out.println(e);
		}
	}
	
	
	
	/*Emprestimo*/
	public Emprestimo solicitarEmprestimo(Cliente cliente, Livro livro) {
		String uri = MICRO_SERVICE_EMPRESTIMO + "/solicitar?emailOrigem=" + cliente.email + "&emailDestino=" + livro.clienteEmail + "&idDoLivro=" + livro.id;
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<Emprestimo> typeRef = new ParameterizedTypeReference<Emprestimo>() {};
		ResponseEntity<Emprestimo> response = restTemplate.exchange(uri, HttpMethod.PUT, null, typeRef);
	    return response.getBody();
	}
	public boolean removerEmprestimo(Emprestimo emprestimo) {
		String uri = MICRO_SERVICE_AMIGO + "/remover?id=" + emprestimo.id;
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<Emprestimo> typeRef = new ParameterizedTypeReference<Emprestimo>() {};
		ResponseEntity<Emprestimo> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, typeRef);
	    return (response.getBody() != null);
	}
	public boolean aceitarEmprestimo(Emprestimo emprestimo) {
		String uri = MICRO_SERVICE_AMIGO + "/aceitar?id=" + emprestimo.id;
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<Emprestimo> typeRef = new ParameterizedTypeReference<Emprestimo>() {};
		ResponseEntity<Emprestimo> response = restTemplate.exchange(uri, HttpMethod.PUT, null, typeRef);
	    return (response.getBody() != null);
	}
	public List<Emprestimo> meusEmprestimos(Cliente cliente) {
		final String uri = MICRO_SERVICE_EMPRESTIMO + "/meusEmprestimos" + "?email=" + cliente.email;
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<List<Emprestimo>> typeRef = new ParameterizedTypeReference<List<Emprestimo>>() {};
		ResponseEntity<List<Emprestimo>> response = restTemplate.exchange(uri, HttpMethod.GET, null, typeRef);
		return response.getBody();
	}
	public List<Emprestimo> listaDeSolicitacoesDeEmprestimos(Cliente cliente) {
		final String uri = MICRO_SERVICE_EMPRESTIMO + "/solicitacoes" + "?emailDestino=" + cliente.email;
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<List<Emprestimo>> typeRef = new ParameterizedTypeReference<List<Emprestimo>>() {};
		ResponseEntity<List<Emprestimo>> response = restTemplate.exchange(uri, HttpMethod.GET, null, typeRef);
		return response.getBody();
	}
	
	/*Amigo*/
	public Amigo solicitarAmizade(String emailOrigem, String emailDestino) {
		String uri = MICRO_SERVICE_AMIGO + "/solicitar?emailOrigem=" + emailOrigem + "&emailDestino=" + emailDestino;
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<Amigo> typeRef = new ParameterizedTypeReference<Amigo>() {};
		ResponseEntity<Amigo> response = restTemplate.exchange(uri, HttpMethod.PUT, null, typeRef);
	    return response.getBody();
	}
	public boolean removerAmizade(Amigo amigo) {
		String uri = MICRO_SERVICE_AMIGO + "/remover?id=" + amigo.id;
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<Amigo> typeRef = new ParameterizedTypeReference<Amigo>() {};
		ResponseEntity<Amigo> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, typeRef);
	    return (response.getBody() != null);
	}
	public boolean aceitarAmizade(Amigo amigo) {
		String uri = MICRO_SERVICE_AMIGO + "/aceitar?id=" + amigo.id;
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<Amigo> typeRef = new ParameterizedTypeReference<Amigo>() {};
		ResponseEntity<Amigo> response = restTemplate.exchange(uri, HttpMethod.PUT, null, typeRef);
	    return (response.getBody() != null);
	}
	public List<Amigo> getListaDeAmigos(Cliente cliente) {
		final String uri = MICRO_SERVICE_AMIGO + "/meusAmigos" + "?email=" + cliente.email;
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<List<Amigo>> typeRef = new ParameterizedTypeReference<List<Amigo>>() {};
		ResponseEntity<List<Amigo>> response = restTemplate.exchange(uri, HttpMethod.GET, null, typeRef);
		return response.getBody();
	}
	public List<Amigo> getListaSolicitacoesDeAmizade(Cliente cliente) {
		final String uri = MICRO_SERVICE_AMIGO + "/solicitacoes" + "?emailDestino=" + cliente.email;
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<List<Amigo>> typeRef = new ParameterizedTypeReference<List<Amigo>>() {};
		ResponseEntity<List<Amigo>> response = restTemplate.exchange(uri, HttpMethod.GET, null, typeRef);
		return response.getBody();
	}
	
	
	/*Livros*/
	public boolean adicionarLivro(Livro livro, Cliente cliente) {
		livro.clienteEmail = cliente.email;
		String uri = MICRO_SERVICE_LIVRO + "/criar?titulo=" + livro.titulo + "&autor=" + livro.autor + "&editora=" + livro.editora + "&clienteEmail=" + livro.clienteEmail;
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<Livro> typeRef = new ParameterizedTypeReference<Livro>() {};
		ResponseEntity<Livro> response = restTemplate.exchange(uri, HttpMethod.PUT, null, typeRef);
	    return (response.getBody() != null);
	}
	public boolean removerLivro(Livro livro) {
		String uri = MICRO_SERVICE_LIVRO + "/remover?id=" + livro.id;
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<Livro> typeRef = new ParameterizedTypeReference<Livro>() {};
		ResponseEntity<Livro> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, typeRef);
	    return (response.getBody() != null);
	}
	public Livro getLivro(String id) {
		final String uri = MICRO_SERVICE_LIVRO + "/" + "?id=" + id;
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<Livro> typeRef = new ParameterizedTypeReference<Livro>() {};
		ResponseEntity<Livro> response = restTemplate.exchange(uri, HttpMethod.GET, null, typeRef);
		return response.getBody();
	}
	public List<Livro> meusLivros(Cliente cliente) {
		final String uri = MICRO_SERVICE_LIVRO + "/porCliente" + "?clienteEmail=" + cliente.email;
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<List<Livro>> typeRef = new ParameterizedTypeReference<List<Livro>>() {};
		ResponseEntity<List<Livro>> response = restTemplate.exchange(uri, HttpMethod.GET, null, typeRef);
		return response.getBody();
	}
	public List<Livro> buscarLivro(String titulo) {
		final String uri = MICRO_SERVICE_LIVRO + "/encontrar" + "?titulo=" + titulo;
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<List<Livro>> typeRef = new ParameterizedTypeReference<List<Livro>>() {};
		ResponseEntity<List<Livro>> response = restTemplate.exchange(uri, HttpMethod.GET, null, typeRef);
		return response.getBody();
	}
	public List<Livro> getTodosOsLivros() {
		final String uri = MICRO_SERVICE_LIVRO + "/todos";
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<List<Livro>> typeRef = new ParameterizedTypeReference<List<Livro>>() {};
		ResponseEntity<List<Livro>> response = restTemplate.exchange(uri, HttpMethod.GET, null, typeRef);
		return response.getBody();
	}
	
	
	/*Clientes*/
	public boolean criarCliente(String name, String email) {
		String uri = MICRO_SERVICE_CLIENTE + "/criar?name=" + name + "&email=" + email;
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<Cliente> typeRef = new ParameterizedTypeReference<Cliente>() {};
		ResponseEntity<Cliente> response = restTemplate.exchange(uri, HttpMethod.PUT, null, typeRef);
	    return (response.getBody() != null);
	}
	public Cliente getCliente(String email) {
		String uri = MICRO_SERVICE_CLIENTE + "/encontrar" + "?email=" + email;
		RestTemplate restTemplate = new RestTemplate();
	    Cliente result = restTemplate.getForObject(uri, Cliente.class);
	    return result;
	}
	public List<Cliente> getTodosOsClientes() {
		final String uri = MICRO_SERVICE_CLIENTE + "/todos";
		RestTemplate restTemplate = new RestTemplate();
		ParameterizedTypeReference<List<Cliente>> typeRef = new ParameterizedTypeReference<List<Cliente>>() {};
		ResponseEntity<List<Cliente>> response = restTemplate.exchange(uri, HttpMethod.GET, null, typeRef);
		return response.getBody();
	}
}
