package com.fferreira3d;

import org.springframework.data.annotation.Id;

public class Amigo {
	@Id
	public String id;
	
	public String emailOrigem;
	public String emailDestino;
	public boolean solicitacao;
	public boolean confirmado;
	
	public Amigo() {}
	
	public Amigo(String emailOrigem, String emailDestino) {
	    this.emailOrigem = emailOrigem;
	    this.emailDestino = emailDestino;
	    solicitacao = true;
	    confirmado = false;
	}
	
	@Override
	public String toString() {
	    return String.format("Amigo[id=%s, emailOrigem='%s', emailDestino='%s', solicitacao='%s', confirmado='%s']", id, emailOrigem, emailDestino, solicitacao, confirmado);
	}
}
