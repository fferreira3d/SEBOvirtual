package com.fferreira3d;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface AmigoRepository extends MongoRepository<Amigo, String> {
    public Amigo findById(String id);
    public List<Amigo> findByEmailDestinoAndSolicitacaoIsTrue(String emailDestino); //Solicitações de amizade pendentes
    public Amigo findByEmailOrigemAndEmailDestino(String emailOrigem, String emailDestino); //Solicitação/amizade já existe
    public List<Amigo> findByEmailOrigemAndConfirmadoIsTrue(String emailOrigem); //Lista de amigos confirmados e solicitados pelo Origem
    public List<Amigo> findByEmailDestinoAndConfirmadoIsTrue(String emailDestino); //Lista de amigos confirmados e solicitados pelo Destino
}
