package com.fferreira3d;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSeboAmigoApplication  implements CommandLineRunner {
	@Autowired
	private AmigoRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(SpringSeboAmigoApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		System.out.println("---------------------------------");
		System.out.println("SEBO VIRTUAL - MicroServiço AMIGO");
		System.out.println("---------------------------------");
		
		//repository.deleteAll();
		
		System.out.println("Amigos encontrados por findAll():");
		System.out.println("---------------------------------");
		for (Amigo amigo : repository.findAll()) {
			System.out.println(amigo);
		}
		System.out.println();

	}
}
