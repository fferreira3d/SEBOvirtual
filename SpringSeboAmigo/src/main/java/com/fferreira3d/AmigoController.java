package com.fferreira3d;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/amigo")
public class AmigoController {
	@Autowired
	private AmigoRepository repository;
	
	@ResponseBody
	@RequestMapping(value = "/solicitar", method = RequestMethod.PUT)
    public Amigo solicitarAmizade(@RequestParam(value="emailOrigem", defaultValue="") String emailOrigem,
    		@RequestParam(value="emailDestino", defaultValue="") String emailDestino) {
		
		Amigo amigo = new Amigo(emailOrigem,emailDestino);
    	if(repository.findByEmailOrigemAndEmailDestino(emailOrigem,emailDestino) == null) {
    		repository.save(amigo);
    	}
		return amigo;
    }
	
	@ResponseBody
	@RequestMapping(value = "/remover", method = RequestMethod.DELETE)
    public Amigo removerAmizade(@RequestParam(value="id", defaultValue="") String id) {
    	Amigo amigo = repository.findById(id);
    	if(amigo != null) {
    		repository.delete(amigo);
    	}
		return amigo;
    }
	
	@ResponseBody
	@RequestMapping(value = "/aceitar", method = RequestMethod.PUT)
    public Amigo aceitarAmizade(@RequestParam(value="id", defaultValue="") String id) {
    	Amigo amigo = repository.findById(id);
    	if(amigo != null) {
    		amigo.solicitacao = false;
    		amigo.confirmado = true;
    		repository.save(amigo);
    	}
		return amigo;
    }
	

	@ResponseBody
	@RequestMapping(value = "/meusAmigos", method = RequestMethod.GET)
    public List<Amigo> listaDeAmigos(@RequestParam(value="email", defaultValue="") String email) {
		List<Amigo> listaTotalDeAmigos = repository.findByEmailDestinoAndConfirmadoIsTrue(email);
		listaTotalDeAmigos.addAll(repository.findByEmailOrigemAndConfirmadoIsTrue(email));
		return listaTotalDeAmigos;
    }
	
	@ResponseBody
	@RequestMapping(value = "/solicitacoes", method = RequestMethod.GET)
    public List<Amigo> listaDeSolicitacoes(@RequestParam(value="emailDestino", defaultValue="") String emailDestino) {
		return repository.findByEmailDestinoAndSolicitacaoIsTrue(emailDestino);
    }
}
