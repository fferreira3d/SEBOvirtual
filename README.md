# Laboratório PUC - Oferta 5 - PPDS - Aula 02
###### Curso de Arquitetura de Software Distribuído - Oferta 5
###### Disciplina de Produtividade no Desenvolvimento de Software

----
# Respostas ao roteiro de atividades


### Questão 1 - Comparação entre arquitetura de microserviços e arquitetura monolitica:
##### Granulação
Através de micro-serviços é possível adaptar a granularidade para as necessidades do cliente, utilizando-se de API-gateway é possível fornecer diferentes formas de requisição a um determinado dado. Já em uma aplicação monolítica isto não é possível, pois cada requisição HTTP passa por um balanceador de cargas que direciona a requisição às N instancias idênticas da aplicação.

##### Gestão de configuração e Componentização
Nas aplicações monolíticas os componentes são empacotados na aplicação, de forma que será necessário o build de toda a aplicação para qualquer alteração em apenas um dos componentes. No modelo de micro-serviços cada componente da aplicação monolítica foi transformada em um micro-serviço independente de forma que futuras alterações serão tratadas individualmente para cada micro-serviço.

##### Organização do modelo de dados
Em uma aplicação monolítica o controle da base de dados levando em conta a consistência e disponibilidade é muitas vezes trivial para determinadas regras de negocio. Já no modelo de micro-serviços, cada serviço possui sua própria base de dados, levando a um baixo acoplamento, contudo torna-se difícil o tratamento de solicitações a dados pertencentes a vários serviços.

##### Controle transacional
No modelo de micro-serviços há a dificuldade de atualização de dados de diferentes serviços. O controle por transações distribuídas, onde um serviço atualiza determinado dado com o outro serviço, garante a consistência mas reduz a disponibilidade, pois todos os serviços envolvidos dever estar disponíveis para a conclusão da requisição. Outra abordagem é o uso de eventos que anunciam as alterações em determinado dado, aumenta a disponibilidade mas não garante a consistência devido à abordagem assíncrona dos eventos.

##### Implantabilidade
Para aplicações monolíticas pequenas (ou de menor complexidade) a implantação é simples, contudo quando a aplicação cresce a entrega continua torna-se mais difícil. Mesmo as IDE sendo voltadas ao desenvolvimento de aplicações monolíticas, a arquitetura de micro-serviços  permite um desenvolvimento mais rápido devido à implementação independente dos módulos (serviços)
Presença de um servidor Web/aplicação
Na arquitetura monolítica a aplicação é executada dentro do servidor web, podendo ser clusterizada aumentando a escalabilidade do sistema. Na arquitetura de micro-serviços cada serviço poderá estar em servidores distintos, além de poder ser gerenciada por um servidor de front-end denominado API gateway que permite a granularidade adaptada para as necessidades do cliente.

##### Diversidade de linguagens e APIs de programação
Na arquitetura de micro-serviços, cada serviço pode ser desenvolvido em uma tecnologia diferente, garantindo maior liberdade, levando às escolhas mais inteligentes para a resolução de determinados tipos de problema.


### Questão 2 - Breve descrição de anotações da ferramenta SpringBoot:
* Através da anotação @controller diz a minha aplicação que ela ira receber requisições.
* O caminho de cada requisição é configurado pela anotação @RequestMapping.
* A anotação @ResponseBody diz ao Spring que o método ira retornar uma mensagem ao REST.

### Questão 3 - Diagrama UML2 do projeto desenvolvido (SEBO virtual):
![DiagramaUML](docs/DiagramaUML.jpg)

### Questão 4 - SEBO virtual microserviços:
Foi desenvolvido 4 microserviços, cada um tratando de um tema especifico da aplicação (Clientes, Livros, Amigos e Emprestimos). Estes serviços são consumidos via REST por uma Aplicação Web (para este projeto ainda não foi desenvolvido a parte de view, apenas rotinas de consumo aos microserviços, de forma a validar o contexto proposto).
A persistencia é feita com MongoDB em todos os quatro serviços, cada serviço com sua base de dados independente. O serviços foram desenvolvidos utilizando se da tecnologia SpringBoot na ferramenta [Eclipse](https://spring.io/tools)


| ***Requisições REST*** | | | |
|:---|:---:|:---:|:---|
| **Request Service CLIENTE** | **Method** | **Params** | **Description** |
| http://localhost:8888/cliente/criar | ***PUT*** | name / email | Cria um novo Cliente |
| http://localhost:8888/cliente/alterar | ***POST*** | id / name /  email | Altera o nome ou email de determinado Cliente |
| http://localhost:8888/cliente/remover | ***DELETE*** | id / email | Apaga determinado Cliente pelo id ou email |
| http://localhost:8888/cliente/encontrar | ***GET*** | email | Retorna o objeto Cliente |
| http://localhost:8888/cliente/todos | ***GET*** |  |  Retorna a lista de todos os Clientes |
| | | | |
| **Request Service LIVRO** | **Method** | **Params** | **Description** |
| http://localhost:9999/livro | ***GET*** | id | Retorna o objeto Livro |
| http://localhost:9999/criar | ***PUT*** | titulo / autor / editora / clienteEmail | Adiciona um novo Livro |
| http://localhost:9999/alterar | ***POST*** | id / titulo / autor / editora | Altera algum parâmetro de um Livro |
| http://localhost:9999/remover | ***DELETE*** | id | Remove um Livro |
| http://localhost:9999/encontrar | ***GET*** | titulo | Retorna a lista de Livros que contenham a palavra chave |
| http://localhost:9999/porCliente | ***GET*** | clienteEmail | Retorna a lista de Livros de um Cliente |
| http://localhost:9999/todos | ***GET*** |  | Retorna a lista de todos os Livros |
| | | | |
| **Request Service AMIGO** | **Method** | **Params** | **Description** |
| http://localhost:7777/amigo/solicitar | ***PUT*** | emailOrigem / emailDestino | Cria uma solicitação de amizade |
| http://localhost:7777/amigo/remover | ***DELETE*** | id  | Remove uma solicitação ou amizade |
| http://localhost:7777/amigo/aceitar | ***PUT*** | id | Aceita uma solicitação de amizade |
| http://localhost:7777/amigo/meusAmigos | ***GET*** | email | Retorna a lista de Amigos |
| http://localhost:7777/amigo/solicitacoes | ***GET*** | emailDestino |  Retorna a lista de solicitações de amizade |
| | | | |
| **Request Service EMPRESTIMO** | **Method** | **Params** | **Description** |
| http://localhost:6666/emprestimo/solicitar | ***PUT*** | emailOrigem / emailDestino / idDoLivro | Cria uma solicitação de Emprestimo |
| http://localhost:6666/emprestimo/remover | ***DELETE*** | id | Remove uma solicitação ou Emprestimo |
| http://localhost:6666/emprestimo/aceitar | ***PUT*** | id |Aceita uma solicitação de Emprestimo |
| http://localhost:6666/emprestimo/meusEmprestimos | ***GET*** | email | Retorna a lista de empréstimos |
| http://localhost:6666/emprestimo/solicitacoes | ***GET*** | emailDestino |  Retorna a lista de solicitações de Emprestimo |