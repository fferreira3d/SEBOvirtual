package com.fferreira3d;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSeboEmprestimoApplication implements CommandLineRunner {
	@Autowired
	private EmprestimoRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(SpringSeboEmprestimoApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		System.out.println("--------------------------------------");
		System.out.println("SEBO VIRTUAL - MicroServiço EMPRESTIMO");
		System.out.println("--------------------------------------");
		
		//repository.deleteAll();
		
		System.out.println("Emprestimos encontrados por findAll():");
		System.out.println("--------------------------------------");
		for (Emprestimo emprestimo : repository.findAll()) {
			System.out.println(emprestimo);
		}
		System.out.println();

	}
}
