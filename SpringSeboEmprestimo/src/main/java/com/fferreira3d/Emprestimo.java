package com.fferreira3d;

import org.springframework.data.annotation.Id;

public class Emprestimo {
	@Id
	public String id;
	
	public String emailOrigem;
	public String emailDestino;
	public String idDoLivro;
	public boolean solicitacao;
	public boolean confirmado;
	
	public Emprestimo() {}
	
	public Emprestimo(String emailOrigem, String emailDestino, String idDoLivro) {
	    this.emailOrigem = emailOrigem;
	    this.emailDestino = emailDestino;
	    this.idDoLivro = idDoLivro;
	    solicitacao = true;
	    confirmado = false;
	}
	
	@Override
	public String toString() {
	    return String.format("Emprestimo[id=%s, emailOrigem='%s', emailDestino='%s', idDoLivro='%s', solicitacao='%s', confirmado='%s']", id, emailOrigem, emailDestino, idDoLivro, solicitacao, confirmado);
	}
}
