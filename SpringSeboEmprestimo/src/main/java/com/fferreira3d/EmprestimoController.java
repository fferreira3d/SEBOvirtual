package com.fferreira3d;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/emprestimo")
public class EmprestimoController {
	@Autowired
	private EmprestimoRepository repository;
	
	@ResponseBody
	@RequestMapping(value = "/solicitar", method = RequestMethod.PUT)
    public Emprestimo solicitarAmizade(@RequestParam(value="emailOrigem", defaultValue="") String emailOrigem,
    		@RequestParam(value="emailDestino", defaultValue="") String emailDestino,
    		@RequestParam(value="idDoLivro", defaultValue="") String idDoLivro) {
		
		Emprestimo emprestimo = new Emprestimo(emailOrigem,emailDestino,idDoLivro);
    	if(repository.findByEmailOrigemAndEmailDestinoAndIdDoLivro(emailOrigem,emailDestino,idDoLivro) == null) {
    		repository.save(emprestimo);
    	}
		return emprestimo;
    }
	
	@ResponseBody
	@RequestMapping(value = "/remover", method = RequestMethod.DELETE)
    public Emprestimo removerAmizade(@RequestParam(value="id", defaultValue="") String id) {
    	Emprestimo emprestimo = repository.findById(id);
    	if(emprestimo != null) {
    		repository.delete(emprestimo);
    	}
		return emprestimo;
    }
	
	@ResponseBody
	@RequestMapping(value = "/aceitar", method = RequestMethod.PUT)
    public Emprestimo aceitarAmizade(@RequestParam(value="id", defaultValue="") String id) {
    	Emprestimo emprestimo = repository.findById(id);
    	if(emprestimo != null) {
    		emprestimo.solicitacao = false;
    		emprestimo.confirmado = true;
    		repository.save(emprestimo);
    	}
		return emprestimo;
    }
	

	@ResponseBody
	@RequestMapping(value = "/meusEmprestimos", method = RequestMethod.GET)
    public List<Emprestimo> listaDeEmprestimos(@RequestParam(value="email", defaultValue="") String email) {
		List<Emprestimo> listaTotalDeEmprestimos = repository.findByEmailDestinoAndConfirmadoIsTrue(email);
		listaTotalDeEmprestimos.addAll(repository.findByEmailOrigemAndConfirmadoIsTrue(email));
		return listaTotalDeEmprestimos;
    }
	
	@ResponseBody
	@RequestMapping(value = "/solicitacoes", method = RequestMethod.GET)
    public List<Emprestimo> listaDeSolicitacoes(@RequestParam(value="emailDestino", defaultValue="") String emailDestino) {
		return repository.findByEmailDestinoAndSolicitacaoIsTrue(emailDestino);
    }
}
