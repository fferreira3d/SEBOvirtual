package com.fferreira3d;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface EmprestimoRepository extends MongoRepository<Emprestimo, String> {
	public Emprestimo findById(String id);
    public List<Emprestimo> findByEmailDestinoAndSolicitacaoIsTrue(String emailDestino); //Solicitações de emprestimo pendentes
    public Emprestimo findByEmailOrigemAndEmailDestinoAndIdDoLivro(String emailOrigem, String emailDestino, String idDoLivro); //Solicitação/emprestimo já existe
    public List<Emprestimo> findByEmailOrigemAndConfirmadoIsTrue(String emailOrigem); //Lista de solicitações feitas de emprestimos confirmados
    public List<Emprestimo> findByEmailDestinoAndConfirmadoIsTrue(String emailDestino); //Lista de solicitações recebidas emprestimos confirmados
}
