package com.fferreira3d;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/cliente")
public class ClienteController {
	@Autowired
	private ClienteRepository repository;
	
	private static final String jsonTemplate = "{ \"%s\": \"%s\" }";

	@ResponseBody
	@RequestMapping(value = "/criar", method = RequestMethod.PUT)
    public Cliente criarCliente(@RequestParam(value="name", defaultValue="") String name, @RequestParam(value="email", defaultValue="") String email) {
		Cliente cliente = null;
		if(repository.findByEmail(email) == null) {
			cliente = new Cliente(name,email);
			repository.save(cliente);
    	}
		return cliente;
    }
	
	@RequestMapping(value = "/alterar", method = RequestMethod.POST)
    public Cliente alterarCliente(@RequestParam(value="id", defaultValue="") String id, @RequestParam(value="name", defaultValue="") String name, @RequestParam(value="email", defaultValue="") String email) {
    	Cliente cliente = repository.findById(id);
    	if(cliente != null) {
    		if(email.contains("@"))
    			cliente.email = email;
    		if(!name.equalsIgnoreCase(""))
    			cliente.name = name;
    		repository.save(cliente);
    	}
		return cliente;
    }
	
	@RequestMapping(value = "/remover", method = RequestMethod.DELETE)
    public Cliente removerCliente(@RequestParam(value="id", defaultValue="") String id, @RequestParam(value="email", defaultValue="") String email) {
    	Cliente cliente = null;
    	if(!id.equalsIgnoreCase(""))
    		cliente = repository.findById(id);
    	if(cliente == null && email.length() > 0)
    		cliente = repository.findByEmail(email);
    	
    	if(cliente != null) {
    		repository.delete(cliente);
    	}
		return cliente;
    }
	
	@ResponseBody
	@RequestMapping(value = "/encontrar", method = RequestMethod.GET)
    public Cliente getCliente(@RequestParam(value="email", defaultValue="") String email) {
    	return repository.findByEmail(email);
    }
	
	@ResponseBody
	@RequestMapping(value = "/todos", method = RequestMethod.GET)
    public List<Cliente> getClientes() {
    	return repository.findAll();
    }
}
