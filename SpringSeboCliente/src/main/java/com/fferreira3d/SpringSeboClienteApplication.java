package com.fferreira3d;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSeboClienteApplication implements CommandLineRunner {
	@Autowired
	private ClienteRepository repository;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringSeboClienteApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		System.out.println("-----------------------------------");
		System.out.println("SEBO VIRTUAL - MicroServiço CLIENTE");
		System.out.println("-----------------------------------");
		
		//repository.deleteAll();
		
		System.out.println("Clientes encontrados por findAll():");
		System.out.println("-----------------------------------");
		for (Cliente cliente : repository.findAll()) {
			System.out.println(cliente);
		}
		System.out.println();

	}
}
