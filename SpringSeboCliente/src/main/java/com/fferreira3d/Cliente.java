package com.fferreira3d;

import org.springframework.data.annotation.Id;

public class Cliente {
	@Id
	public String id;
	
	public String name;
	public String email;
	
	public Cliente() {}
	
	public Cliente(String name, String email) {
	    this.name = name;
	    this.email = email;
	}
	public Cliente(String id, String name, String email) {
		this.id = id;
		this.name = name;
	    this.email = email;
	}
	
	@Override
	public String toString() {
	    return String.format(
	            "Cliente[id=%s, name='%s', email='%s']",
	            id, name, email);
	}
}
