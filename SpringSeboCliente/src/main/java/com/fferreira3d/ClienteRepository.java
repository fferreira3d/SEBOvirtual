package com.fferreira3d;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ClienteRepository extends MongoRepository<Cliente, String> {
    public Cliente findByEmail(String email);
    public Cliente findById(String id);
    public List<Cliente> findByName(String name);

}