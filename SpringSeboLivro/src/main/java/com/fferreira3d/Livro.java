package com.fferreira3d;

import org.springframework.data.annotation.Id;

public class Livro {
	@Id
	public String id;
	
	public String titulo;
	public String autor;
	public String editora;
	public String clienteEmail;
	
	public Livro() {}
	
	public Livro(String titulo, String autor, String editora, String clienteEmail) {
	    this.titulo = titulo;
	    this.autor = autor;
	    this.editora = editora;
	    this.clienteEmail = clienteEmail;
	}
	public Livro(String id, String titulo, String autor, String editora, String clienteEmail) {
		this.id = id;
		this.titulo = titulo;
	    this.autor = autor;
	    this.editora = editora;
	    this.clienteEmail = clienteEmail;
	}
	
	@Override
	public String toString() {
	    return String.format("Livro[id=%s, titulo='%s', autor='%s', editora='%s', clienteid='%s']", id, titulo, autor, editora, clienteEmail);
	}
}
