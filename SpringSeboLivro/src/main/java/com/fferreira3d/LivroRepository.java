package com.fferreira3d;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface LivroRepository extends MongoRepository<Livro, String> {
    public Livro findById(String id);
    public List<Livro> findByTitulo(String titulo);
    public Livro findByTituloAndAutorAndEditoraAndClienteEmail(String titulo,String autor,String editora,String clienteEmail);
    public List<Livro> findByClienteEmail(String clienteEmail);
    public List<Livro> findByTituloContaining(String titulo);
}
