package com.fferreira3d;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSeboLivroApplication implements CommandLineRunner {
	@Autowired
	private LivroRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(SpringSeboLivroApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		System.out.println("---------------------------------");
		System.out.println("SEBO VIRTUAL - MicroServiço LIVRO");
		System.out.println("---------------------------------");
		
		//repository.deleteAll();
		
		System.out.println("Livros encontrados por findAll():");
		System.out.println("---------------------------------");
		for (Livro livro : repository.findAll()) {
			System.out.println(livro);
		}
		System.out.println();

	}
}
