package com.fferreira3d;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/livro")
public class LivroController {	
	@Autowired
	private LivroRepository repository;

	@ResponseBody
	@RequestMapping(value = "/", method = RequestMethod.DELETE)
    public Livro getLivro(@RequestParam(value="id", defaultValue="") String id) {
    	return repository.findById(id);
    }
	
	@ResponseBody
	@RequestMapping(value = "/criar", method = RequestMethod.PUT)
    public Livro criarLivro(@RequestParam(value="titulo", defaultValue="") String titulo,
    		@RequestParam(value="autor", defaultValue="") String autor,
    		@RequestParam(value="editora", defaultValue="") String editora,
    		@RequestParam(value="clienteEmail", defaultValue="") String clienteEmail) {
		
    	Livro livro = null;
    	if(!titulo.equalsIgnoreCase("") && !clienteEmail.equalsIgnoreCase("") && repository.findByTituloAndAutorAndEditoraAndClienteEmail(titulo,autor,editora,clienteEmail) == null) {
    		livro = new Livro(titulo,autor,editora,clienteEmail);
    		repository.save(livro);
    	}
		return livro;
    }
	
	@RequestMapping(value = "/alterar", method = RequestMethod.POST)
    public Livro alterarLivro(@RequestParam(value="id", defaultValue="") String id,
    		@RequestParam(value="titulo", defaultValue="") String titulo,
    		@RequestParam(value="autor", defaultValue="") String autor,
    		@RequestParam(value="editora", defaultValue="") String editora) {

    	Livro livro = repository.findById(id);
    	if(livro != null && !titulo.equalsIgnoreCase("")) {
	    	repository.save(new Livro(id, titulo, autor, editora, livro.clienteEmail));
    	}
		return livro;
    }
	
	@RequestMapping(value = "/remover", method = RequestMethod.DELETE)
    public Livro removerLivro(@RequestParam(value="id", defaultValue="") String id) {
    	Livro livro = null;
    	if(!id.equalsIgnoreCase(""))
    		livro = repository.findById(id);
    	
    	if(livro != null) {
    		repository.delete(livro);
    	}
		return livro;
    }
	
	@ResponseBody
	@RequestMapping(value = "/encontrar", method = RequestMethod.GET)
    public List<Livro> encontrarLivro(@RequestParam(value="titulo", defaultValue="") String titulo) {
    	return repository.findByTituloContaining(titulo);
    }
	
	@ResponseBody
	@RequestMapping(value = "/porCliente", method = RequestMethod.GET)
    public List<Livro> porClienteIdLivros(@RequestParam(value="clienteEmail", defaultValue="") String clienteEmail) {
    	return repository.findByClienteEmail(clienteEmail);
    }
	
	@ResponseBody
	@RequestMapping(value = "/todos", method = RequestMethod.GET)
    public List<Livro> todosOsLivros() {
    	return repository.findAll();
    }
}
